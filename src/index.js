'use strict';
const puppeteer = require('puppeteer');
const {URL} = require('url');
const fse = require('fs-extra');
const perf = require('execution-time')();

// page not visited
const noVisited = new Set();
// page visited
const alreadyVisited = new Set();

// format url for regex
const quoteRegex = function(str, replace = '\\$1') {
  return str.replace(/([.?*+^$[\]\\(){}|-])/g, replace);
};
// clean url for file name
const cleanName = new RegExp(/[\.\/\:-?{}()#*\+\\|]/g); // eslint-disable-line

// extension blocked
const extensionRegex = new RegExp(
  '.(xml|less|png|jpg|jpeg|gif|pdf|doc|txt|ico|rss|zip|mp3|rar|exe|wmv|doc|avi|ppt|mpg|mpeg|tif|wav|mov|psd|ai|xls|mp4|m4a|swf|dat|dmg|iso|flv|m4v|torrent|ttf|woff|woff2)$',
);

// js external
const tiersRegex = new RegExp(
  '(googletagmanager|google-analytics|googleapis|facebook|twitter|zopim)',
);

async function crawlWebsite(urlToFetch) {
  perf.start('site');
  const browser = await puppeteer.launch({
    headless: true,
    args: ['--no-sandbox', '--headless'],
  });

  // url base
  const url = new URL(urlToFetch);
  const rootUrl = url.protocol + '//' + url.host;
  const host = url.hostname;
  const protocol = url.protocol;

  const hostSearch = new RegExp(`^${quoteRegex(protocol + '//' + host)}`);

  async function scrap(urlPage) {
    perf.start('page');
    // open new page
    const page = await browser.newPage();
    // intercept request for bloc some formats
    await page.setRequestInterception(true);
    page.on('request', req => {
      if (
        // ['image', 'font'].indexOf(req.resourceType()) > -1 ||
        req.url().search(extensionRegex) > -1 ||
        req.url().search(tiersRegex) > -1
      ) {
        return req.abort();
      }
      req.continue();
    });

    try {
      // open page
      await page.goto(urlPage, {waitUntil: 'networkidle0'});
    } catch (e) {
      console.log(e); // eslint-disable-line
    }

    // catch all href link from page open
    const allHref = await page.evaluate(() => {
      // eslint-disable-next-line
      return Array.from(document.getElementsByTagName('a')).map(node => {
        return node.href;
      });
    });

    // filter url
    const filters = () =>
      allHref
        // keep only url from same domain
        .filter(a => a.search(hostSearch) > -1)
        // remove all request for jpg, pdf, etc.
        .filter(a => a.search(extensionRegex) === -1)
        .map(e => {
          const u = new URL(e);
          // reconstruct clean url
          return `${u.protocol}//${u.host}${u.pathname}${u.search}`.replace(
            /\/$/,
            '',
          );
        });

    // set link found in notVisited only if already not visited before
    filters().forEach(e => {
      if (alreadyVisited.has(e) === false) {
        noVisited.add(e);
      }
    });

    // get content page
    const content = await page.content();

    const urlScrap = new URL(urlPage);
    // dir name
    const dirName = urlScrap.hostname.replace(cleanName, '-');
    // format url for file name
    const fileName = `${dirName}${urlScrap.pathname}${urlScrap.search}`.replace(
      cleanName,
      '-',
    );

    // const fileName = urlPage.replace(cleanName, '-');
    // save content in file
    fse.outputFileSync(
      `${__dirname}/../cache/${dirName}/${fileName}.html`,
      content,
    );

    // add page visited
    alreadyVisited.add(urlPage);
    // remove page visited
    noVisited.delete(urlPage);
    // close page
    await page.close();

    const results = perf.stop('page');
    console.log(`${urlPage} visited | ${results.time.toFixed(2)}ms`); // eslint-disable-line
    // noVisited not empty call new page or end
    if (noVisited.size > 0) {
      return scrap(noVisited.values().next().value);
    } else {
      return true;
    }
  }
  // first page open
  await scrap(rootUrl);
  const results = perf.stop('site');
  console.log(`website ${rootUrl} visited in ${results.time.toFixed(2)}ms`); // eslint-disable-line
  browser.close();
}

module.exports = crawlWebsite;
